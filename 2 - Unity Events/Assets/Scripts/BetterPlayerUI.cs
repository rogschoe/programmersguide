﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BetterPlayerUI : MonoBehaviour {

    [SerializeField]
    private BetterPlayer betterPlayer = null;
    [SerializeField]
    private Slider healthBar = null;

    private void Start()
    {
        betterPlayer.OnHealthChanged.AddListener(UpdateHealthBar);
        UpdateHealthBar(betterPlayer.Health);
    }

    void UpdateHealthBar(float currentHealth)
    {
        healthBar.value = currentHealth / betterPlayer.MaxHealth;
    }

}
