﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BadPlayerUI : MonoBehaviour {

    //LETS JUST MAKE EVERYTHING PUBLIC SO EVERYONE
    //CAN USE AND MODIFY MY VALUES WITHOUT MY PERMISSION
    //BECAUSE THIS CANNOT POSSIBLY CAUSE GAME BREAKING BUGS
    //LATER ON. -gasp for air-
    public BadPlayer badPlayer = null;
    public Slider healthBar = null;

	// Update is called once per frame
	void Update () {
        healthBar.value = badPlayer.health / badPlayer.maxHealth;
	}

}
