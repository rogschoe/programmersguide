﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FloatEvent : UnityEvent<float>
{
}

/// <summary>
/// THIS IS HOW A THOUGHTFUL PROGRAMMER MAKES A HEALTH SYSTEM.
/// </summary>
public class BetterPlayer : MonoBehaviour {

    #region Inspector Fields

    [SerializeField]
    private float maxHealth = 10;
    [SerializeField]
    private UnityEvent onDeath;

    //You must end a region
    #endregion

    #region Internal Fields

    private bool isDead = false;
    private float health = 0;
    private FloatEvent onHealthChanged = new FloatEvent();

    #endregion

    #region Properties

    //We only want to get the max health value, not modify it.
    //So I created a getter and left out the setter.
    public float MaxHealth
    {
        get { return maxHealth; }
    }

    //This is a great way to expose the health variable. It allows us to
    //control how the health variable changes and what occurs when we set it.
    //We can also set the setter to be private and allow the getter to be public,
    //this essentially makes the value 'Read Only'
    //
    //By convention, we capitalize the property name and keep the variable lowercase.
    public float Health
    {
        get { return health; }
        set
        {
            //Check if the health value is now different
            if (health != value)
                //Trigger the event
                onHealthChanged.Invoke(value);

            //Ensure health stays between 0 and max health
            health = Mathf.Clamp(value, 0, maxHealth);
            Debug.Log(name + "'s health is now " + health);

            if(health <= 0)
            {
                if(!isDead)
                    IsDead = true;
            }
        }
    }

    public bool IsDead
    {
        get { return isDead; }
        //I make the setter private since nothing else, but the player
        //should change this value. If I want to insta kill a player, I should
        //just subtract the max health from the Health property. 
        private set
        {
            Debug.Log("IsDead property's set function was called!");

            //If the previous isDead value was false and it's now true,
            //then let ther user know that he died.
            if (!isDead && value)
            {
                Debug.Log(name + " is dead!");
                onDeath.Invoke();
            }

            isDead = value;   
        }
    }

    public FloatEvent OnHealthChanged
    {
        get { return onHealthChanged; }
    }

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        //Ensure that health is maxed out on start
        health = maxHealth;
    }

    // Update is called once per frame
    void Update () {
		//HAHA WE DON'T NEED THIS ANYMORE FOR CHECKING IF THE PLAYER IS DEAD!!

        if(Input.GetKeyDown(KeyCode.A))
        {
            //I subtract one hp from the Health PROPERTY. Not the variable. 
            Health--;
        }
	}

    #endregion
}
