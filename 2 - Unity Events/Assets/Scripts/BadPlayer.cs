﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// THIS IS HOW A NEWB CODES A HEALTH SYSTEM FOR A PLAYER
/// </summary>
public class BadPlayer : MonoBehaviour {

    //LETS JUST MAKE EVERYTHING PUBLIC SO EVERYONE
    //CAN USE AND MODIFY MY VALUES WITHOUT MY PERMISSION
    //BECAUSE THIS CANNOT POSSIBLY CAUSE GAME BREAKING BUGS
    //LATER ON. -gasp for air-
    public float health = 10;
    public float maxHealth = 10;
    public bool isDead = false;

	// Update is called once per frame
	void Update () {
		//Every SINGLE frame check if the player has no 
        //more health and is not already dead
        if(health <= 0 && !isDead)
        {
            //The player finally died after so many checks
            //every SINGLE frame
            Debug.Log("Oh " + name + " is dead now, BUT Unity still has to check if I am dead every SINGLE frame!");

            //Set the player to be dead
            isDead = true;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            //I subtract one hp from the health variable.
            health--;
            health = Mathf.Clamp(health, 0, maxHealth);
            //I have to put this here instead of the awesome Health property in BetterPlayer. 
            Debug.Log(name + "'s health is now " + health);
        }
    }
}
