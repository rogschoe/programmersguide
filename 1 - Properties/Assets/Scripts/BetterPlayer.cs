﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// THIS IS HOW A THOUGHTFUL PROGRAMMER MAKES A HEALTH SYSTEM.
/// </summary>
public class BetterPlayer : MonoBehaviour {

    //Since we care about the integrity of our system, and keeping it
    //from blowing up into flames, let's make our fields private and 
    //tag the variables with SerializeField that we want the designers to change. 

    //Regions are a great way to organize variables and methods
    #region Inspector Fields

    [SerializeField]
    private float health = 10;

    //You must end a region
    #endregion

    #region Internal Fields

    private bool isDead = false;

    #endregion

    #region Properties

    //This is a great way to expose the health variable. It allows us to
    //control how the health variable changes and what occurs when we set it.
    //We can also set the setter to be private and allow the getter to be public,
    //this essentially makes the value 'Read Only'
    //
    //By convention, we capitalize the property name and keep the variable lowercase.
    public float Health
    {
        get { return health; }
        set
        {
            //The 'value' keyword is what we are passing in
            //when we set the property
            health = value;

            Debug.Log(name + "'s health is now " + health);

            //Check if the health value is now depleted
            //and if the player is still alive. 
            //
            //The takeaway here is that it only checks this when
            //the health's set is called. 
            if(health <= 0 && !isDead)
            {
                //Set the isDead variable to be dead
                //isDead = true;

                //Instead of using the isDead variable directly,
                //let's use the IsDead property so we can have more
                //control over what happens when it changes. 
                IsDead = true;
            }
        }
    }

    public bool IsDead
    {
        get { return isDead; }
        //I make the setter private since nothing else, but the player
        //should change this value. If I want to insta kill a player, I should
        //just subtract the max health from the Health property. 
        private set
        {
            Debug.Log("IsDead property's set function was called!");

            //If the previous isDead value was false and it's now true,
            //then let ther user know that he died.
            if (!isDead && value)
            {
                Debug.Log(name + " is dead!");
            }

            isDead = value;   
        }
    }

    #endregion

    #region MonoBehaviour Methods
	
	// Update is called once per frame
	void Update () {
		//HAHA WE DON'T NEED THIS ANYMORE FOR CHECKING IF THE PLAYER IS DEAD!!

        if(Input.GetKeyDown(KeyCode.A))
        {
            //I subtract one hp from the Health PROPERTY. Not the variable. 
            Health--;
        }
	}

    #endregion
}
